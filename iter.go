package gitlabsping

import (
	"fmt"
	"time"
)

// TimeBoundIterator takes care of iterating and making the call
// reports the data from iterations
type TimeBoundIterator struct {
	timeLimit    time.Duration
	startedAt    time.Time
	counter      int
	respTimeList []int64 // nanoseconds
	avgRespTime  int64
}

// Ping - pipeline to run the ping command and return response time.
func Ping(timeLimit time.Duration, action Action) *TimeBoundIterator {
	t := &TimeBoundIterator{timeLimit: timeLimit, startedAt: time.Now()}
	t.Iterate(action)
	t.reportCalculate()
	return t
}

// Iterate thourgh the atciuon specified and track time details.
func (t *TimeBoundIterator) Iterate(action Action) {
	for t.continueIter() {
		t.counter++
		t.respTimeList = append(t.respTimeList, Benchmark(action).Nanoseconds())
	}
}

func (t TimeBoundIterator) continueIter() bool {
	return time.Since(t.startedAt) < t.timeLimit
}

func (t *TimeBoundIterator) reportCalculate() {
	totalTime := int64(0)
	for i := 0; i < len(t.respTimeList); i++ {
		totalTime += t.respTimeList[i]
	}
	t.avgRespTime = totalTime / int64(len(t.respTimeList))
}

func (t TimeBoundIterator) String() string {
	return fmt.Sprintf("ran %d times - with average response time %s", t.counter, time.Duration(t.avgRespTime))
}
