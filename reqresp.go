package gitlabsping

import (
	"net/http"
	"time"
)

// ensure that NetHttpReq implements interface named Action.
// https://splice.com/blog/golang-verify-type-implements-interface-compile-time/
var _ Action = (*netHTTPReq)(nil)

// Action - anything that has a run method without input output.
type Action interface {
	run()
}

// NewNetHTTPReq returns a new netHTTPReq obj.
func NewNetHTTPReq(url string) Action {
	return netHTTPReq{
		url: url,
	}
}

type netHTTPReq struct {
	url string
}

func (n netHTTPReq) run() {
	http.Get(n.url)
}

// Benchmark - returns time Duration of the Action passed onto it.
func Benchmark(act Action) time.Duration {
	start := time.Now()
	act.run()
	t := time.Now()
	return t.Sub(start)
}
