# Gitlabs Status

Ping Gitlab for 5 minutes and find the response time and report.

### Usage

```
make run ARGS="--help"
```

```
make run ARGS="--dur=1m --url='http://gitlab.com'"
```

response will be of like:

`ran 27 times - with average response time 2.291050984s`

#### Using Docker

```
docker build . -t gitlabsping -f DockerFileRunner
docker run -i -t gitlabsping --help
docker run -i -t gitlabsping --dur=1m --url='https://gitlab.com'
```

build docker image and run help and run the command above using

```
./run.sh
```

Pipeline: [https://gitlab.com/sumitasok/gitlabsping/pipelines](https://gitlab.com/sumitasok/gitlabsping/pipelines)

## Coding standards

_I had been using pre-commit hook as my CI. It is simple._

- instant gratification, as it runs locally.
- because it runs before commit, no code with quality issue is pushed to repository.

Con:
- blocked for all the pipeline to complete, before commiting.

Using hooks provided by https://pre-commit.com/ to do code quality analysis.

After installing pre-commit as described in the above website.

run `pre-commit install` in the repo to install the pre-commit hook, which will check our code quality before commiting.

The pre commit hooks checks using go fmt, go vet, go lint, gometalinter.


ref: [https://github.com/dnephin/pre-commit-golang](https://github.com/dnephin/pre-commit-golang)

#### Gitlab CI

_When Gitlab test project demanded CI, it made me think a better hosted approach to my personal projects._

And I found [pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss](https://medium.com/pantomath/go-tools-gitlab-how-to-do-continuous-integration-like-a-boss-941a3a9ad0b6) interesting.

I have used that in this project. Thus I could try the Gitlab ecosystem.

