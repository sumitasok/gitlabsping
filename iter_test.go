package gitlabsping

import (
	assert "github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestTimeBoundIterator_continueIteration_true(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		timeLimit: 1 * time.Second,
		startedAt: time.Now(),
	}

	time.Sleep(2 * time.Second)

	assert.False(iter.continueIter())
}

func TestTimeBoundIterator_continueIteration_false(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		timeLimit: 4 * time.Second,
		startedAt: time.Now(),
	}

	time.Sleep(1 * time.Second)

	assert.True(iter.continueIter())
}

func TestTimeBoundIterator_Iterate_counter(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		timeLimit: 4 * time.Second,
		startedAt: time.Now(),
	}

	iter.Iterate(timer{1})

	assert.Equal(4, iter.counter)
}

func TestTimeBoundIterator_Iterate_respTimeList(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		timeLimit: 4 * time.Second,
		startedAt: time.Now(),
	}

	iter.Iterate(timer{1})

	assert.True(len(iter.respTimeList) < 5)
}

func TestTimeBoundIterator_reportCalculator(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		respTimeList: []int64{
			int64(2),
			int64(3),
			int64(1),
		},
	}

	iter.reportCalculate()
	assert.Equal(int64(2), iter.avgRespTime)
}

func TestTimeBoundIterator_String(t *testing.T) {
	assert := assert.New(t)

	iter := TimeBoundIterator{
		respTimeList: []int64{
			int64(2),
			int64(3),
			int64(1),
		},
		counter: 7,
	}

	iter.reportCalculate()
	assert.Equal("ran 7 times - with average response time 2ns", iter.String())
}
