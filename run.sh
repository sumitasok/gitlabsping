#!/bin/sh
docker build . -t gitlabsping -f DockerFileRunner
docker run -i -t gitlabsping --help
docker run -i -t gitlabsping --dur=1m --url='https://gitlab.com'
