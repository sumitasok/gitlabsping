package gitlabsping

import (
	assert "github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewNetHTTPReq(t *testing.T) {
	assert := assert.New(t)

	n := NewNetHTTPReq("url")
	assert.NotEmpty(n)
}

var _ Action = (*timer)(nil)

type timer struct {
	dur time.Duration
}

func (t timer) run() {
	time.Sleep(t.dur * time.Second)
}

func TestBenchmark(t *testing.T) {
	assert := assert.New(t)

	assert.Equal(int(Benchmark(timer{1}).Seconds()), int(time.Duration(1*time.Second).Seconds()))
}
