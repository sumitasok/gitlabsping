package main

// package gitlabsping
import (
	"flag"
	"fmt"
	r "gitlab.com/sumitasok/gitlabsping"
	"os"
	"time"
)

var (
	allowedDurationFlag = "Allowed duration strings are '300ms', '-1.5h' or '2h45m'. " +
		"Valid time units are 'ns', 'us' (or 'µs'), 'ms', 's', 'm', 'h'"
)

func main() {
	durPtr := flag.String("dur", "5m", "duration to check the request response time. "+allowedDurationFlag)
	url := flag.String("url", "https://gitlab.com", "url to check the response for.")
	flag.Parse()
	n := r.NewNetHTTPReq(*url)
	t, err := time.ParseDuration(*durPtr)
	if err != nil {
		print(allowedDurationFlag)
		os.Exit(1)
	}
	fmt.Println(r.Ping(t, n))
}
